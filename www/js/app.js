// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.factories', 'starter.filters',
    'google.places', 'ngStorage', 'youtube-embed', 'ngCordova', 'ngDfp', 'angularMoment'
])

    .run(function ($q, DoubleClick, $ionicModal, $ionicLoading, $interval, $filter, $stateParams, $ionicPlatform, $ionicHistory, $rootScope, $localStorage, $http, $timeout, $ionicPopup, $state, $cordovaGeolocation, $ionicSideMenuDelegate) {
        $ionicPlatform.ready(function () {

            // geolocation for ios

            if (ionic.Platform.isIOS() == true){

                if (window.cordova) {

                    document.addEventListener("deviceready", function () {

                        CheckGPS.check(function win() {

                                var posOptions = {timeout: 3000, enableHighAccuracy: true};

                                $cordovaGeolocation
                                    .getCurrentPosition(posOptions)
                                    .then(function (position) {

                                        $rootScope.lat = position.coords.latitude;
                                        $rootScope.lng = position.coords.longitude;
                                        $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                    }, function (err) {

                                        $rootScope.getDealsWithoutLocation();

                                    });

                            },

                            function fail() {

                                $rootScope.getDealsWithoutLocation();

                            });

                    });

                }

            }

            // Back button

            $ionicPlatform.registerBackButtonAction(function (event) {
                if ($state.current.name === 'app.login' || $state.current.name === 'app.home' ||
                    $state.current.name === 'app.inter' || $state.current.name === 'app.questions' ||
                    $state.current.name === 'app.answers' || $state.current.name === 'app.results') {

                    // do nothing

                } else
                    $ionicHistory.goBack();

            }, 100);


            // Notifications and GA

            document.addEventListener("deviceready", function () {

                // Google Analytics

                window.ga.startTrackerWithId('UA-86948163-1');

                // Notifications

                var notificationOpenedCallback = function (jsonData) {

                    //alert(JSON.stringify(jsonData));

                    // NEW MESSAGE FROM SPECIALIST

                    if (jsonData.additionalData.type == "newmessage") {

                        if ($localStorage.userid == '')         // if not logged in
                            $state.go('app.login');

                        else {       // if logged in

                            $rootScope.pushNotificationType = "newmessage";

                            $state.go('app.personal');

                        }

                    }

                    // FIVE QUESTIONS

                    if (jsonData.additionalData.type === "fiveQuestions") {

                        // alert(JSON.stringify(jsonData.additionalData));

                        if ($localStorage.userid == '') {        // if not logged in

                            $state.go('app.login');

                        } else {        // if logged in

                            $http.post($rootScope.host + 'checkUser', {
                                user: $localStorage.userid,
                                date: jsonData.additionalData.date,
                                session: jsonData.additionalData.session_number
                            }, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                                .then(function (data) {

                                    if (data.data.status == '1'){

                                        // if (jsonData.additionalData.bonus == '1' || jsonData.additionalData.bonus == '2'){
                                        //
                                        //     $rootScope.bonus = jsonData.additionalData.bonus;
                                        //
                                        //     if(ionic.Platform.isIOS()) {
                                        //
                                        //         DoubleClick.refreshAds('received500CubeIos');
                                        //         DoubleClick.refreshAds('received500Ios');
                                        //         DoubleClick.refreshAds('received1000CubeIos');
                                        //         DoubleClick.refreshAds('received1000Ios');
                                        //
                                        //     } else if (ionic.Platform.isAndroid()) {
                                        //
                                        //         DoubleClick.refreshAds('received500CubeAndroid');
                                        //         DoubleClick.refreshAds('received500Android');
                                        //         DoubleClick.refreshAds('received1000CubeAndroid');
                                        //         DoubleClick.refreshAds('received1000Android');
                                        //
                                        //     }
                                        //
                                        //     if ($rootScope.banners.length !== 0){
                                        //
                                        //         if ($rootScope.banners.banner_prize1.TipliBannerType === '0'){
                                        //             angular.forEach($rootScope.banners.banner_prize1.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                                        //         }
                                        //
                                        //         if ($rootScope.banners.banner_prize2.TipliBannerType === '0'){
                                        //             angular.forEach($rootScope.banners.banner_prize2.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                                        //         }
                                        //
                                        //         if ($rootScope.banners.cube_prize1.TipliBannerType === '0'){
                                        //             angular.forEach($rootScope.banners.cube_prize1.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                                        //         }
                                        //
                                        //         if ($rootScope.banners.cube_prize2.TipliBannerType === '0'){
                                        //             angular.forEach($rootScope.banners.cube_prize1.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                                        //         }
                                        //     }
                                        // }

                                        $rootScope.getRandomQuestions(jsonData.additionalData.date, jsonData.additionalData.session_number);
                                        $state.go('app.inter', {date: jsonData.additionalData.date, questionId: 0}).then(function () {

                                            // if ($rootScope.bonus == '1' || $rootScope.bonus == '2') {
                                            //
                                            //     $ionicModal.fromTemplateUrl('templates/modal_bonus.html', {scope: $rootScope, animation: 'slide-in-up'})
                                            //
                                            //         .then(function(modal) {
                                            //
                                            //             $rootScope.modal = modal;
                                            //             $rootScope.modal.show();
                                            //         });
                                            //
                                            // }

                                        });

                                    } else {

                                        $rootScope.getUserMonth();

                                        $ionicPopup.alert({title: 'כבר ענית על השאלות של המקבץ הנוכחי'})
                                            .then(function () {$state.go('app.home');})

                                    }

                                }, function (err) {

                                    $ionicPopup.alert({title: 'אין התחברות!'})

                                });

                        }

                    }
                    // BIRTHDAY

                    // if (jsonData.additionalData.type == "birthday") {
                    //
                    //     if ($localStorage.userid == '')        // if not logged in
                    //         $state.go('app.login');
                    //     else {       // if logged in
                    //         $rootScope.getUserMonth();
                    //         $state.go('app.birthday');
                    //     }
                    // }

                    // tip for saturday

                    // if (jsonData.additionalData.type == "shabbat") {
                    //
                    //     if ($localStorage.userid == '')        // if not logged in
                    //         $state.go('app.login');
                    //     else {       // if logged in
                    //         $state.go('app.shabbat');
                    //     }
                    // }

                };

                window.plugins.OneSignal.init("96b66281-ac3d-44e5-834f-e39b3cc98626",
                    {googleProjectNumber: "627358870772"},
                    notificationOpenedCallback);

                window.plugins.OneSignal.getIds(function (ids) {

                    $rootScope.pushId = ids.userId;

                });
                // Show an alert box if a notification comes in when the user is in your app.
                window.plugins.OneSignal.enableInAppAlertNotification(true);

        }, false);

        // Default code

        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);

        }
        if (window.StatusBar) {
            // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }


            // geolocation for Android

            if (window.cordova) {

                $ionicPlatform.ready(function () {

                    CheckGPS.check(function win() {

                            var posOptions = {timeout: 3000, enableHighAccuracy: true};

                            $cordovaGeolocation
                                .getCurrentPosition(posOptions)
                                .then(function (position) {

                                    $rootScope.lat = position.coords.latitude;
                                    $rootScope.lng = position.coords.longitude;
                                    $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                }, function (err) {

                                    $rootScope.getDealsWithoutLocation();

                                });

                        },

                        function fail() {$rootScope.getDealsWithoutLocation();});

                });

            } else {

                var posOptions = {enableHighAccuracy: false};

                $cordovaGeolocation
                    .getCurrentPosition(posOptions)
                    .then(function (position) {

                        $rootScope.lat = position.coords.latitude;
                        $rootScope.lng = position.coords.longitude;

                        $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                    }, function (err) {

                        $rootScope.getDealsWithoutLocation();
                        console.log('err1', err);

                    });

            }

        });

        document.addEventListener("resume", onResume, false);

        function onResume() {

            $rootScope.getUserMonth();

        }

        // global variables

        $rootScope.host = 'http://tapper.org.il/tipli/laravel/public/';
        $rootScope.phpHost = "http://tapper.org.il/tipli/php/";
        $rootScope.imageHost = "http://tapper.org.il/tipli/php/uploads";
        $rootScope.isDailyDealSeen = $localStorage.isDailyDealSeen;
        $rootScope.correctAnswers = 0;
        $rootScope.incorrectAnswers = 0;
        $rootScope.allPoints = 0;
        $rootScope.monthPoints = 0;
        $rootScope.userData = {
            "userid": $localStorage.userid,
            "firstname": $localStorage.firstname,
            "lastname": $localStorage.lastname,
            "password": $localStorage.password,
            "email": $localStorage.email,
            "gender": $localStorage.gender,
            "soldier": $localStorage.soldier,
            "soldier_driver": $localStorage.soldier_driver,
            "gift": $localStorage.gift
        };
        $rootScope.image = $localStorage.image;
        $rootScope.categories = [];
        $rootScope.currState = $state;
        $rootScope.infoCategories = ['רשיון נהיגה', 'תקלות ברכב', 'עשה ואל תעשה', 'במקרה של תאונה', 'מה אומר החוק', 'שאל את המומחה', 'טלפונים חשובים'];
        $rootScope.deals = [];
        $rootScope.closeDeals = [];
        $rootScope.favoriteDeals = [];
        $rootScope.todayDeal = {};
        $rootScope.lat = "";
        $rootScope.lng = "";

        $rootScope.isLocationEnabled = false;
        $rootScope.banners = [];
        $rootScope.questions = [];
        $rootScope.question = null;
        $rootScope.questionId = 0;
        $rootScope.questionNumber = Number($rootScope.questionId) + 1;
        $rootScope.answers = [];
        $rootScope.rightAnswer = '';
        $rootScope.rightAnswers = 0;
        $rootScope.isAnswerCorrect = false;
        $rootScope.divider = 20;
        $rootScope.once = typeof $localStorage.once !== 'undefined' ? $localStorage.once : 1;
        $rootScope.twice = typeof $localStorage.twice !== 'undefined' ? $localStorage.twice : 1;
        $rootScope.points = '00000.0';
        $rootScope.newPoints = 0;
        $rootScope.days = [];
        $rootScope.timeToNext = moment().format('HH:mm');
        $rootScope.prize1 = '0';
        $rootScope.prize2 = '0';
        $rootScope.firstNotAnswered = null;
        $rootScope.session = null;
        $rootScope.dateBeforePopup = '';
        $rootScope.sessionBeforePopup = '';
        $rootScope.isIOS = ionic.Platform.isIOS();
        $rootScope.testQuestions = [];
        $rootScope.testRightAnswers = 0;


        $rootScope.goToDay = function (date, session) {

            $rootScope.beforePopup = $ionicPopup.show({
                templateUrl: 'templates/popup_before.html',
                scope: $rootScope,
                cssClass: 'prizesPopup'
            });

            $rootScope.dateBeforePopup = date;
            $rootScope.sessionBeforePopup = session;

        };

        $rootScope.goToDayAfterPopup = function () {

            console.log($rootScope.modal);

            if (typeof $rootScope.modal !== 'undefined' && $rootScope.modal.isShown()){
                $rootScope.closeModal();
            }

            $rootScope.getRandomQuestions($rootScope.dateBeforePopup, $rootScope.sessionBeforePopup);
            $state.go('app.inter', {date: $rootScope.dateBeforePopup, questionId: 0});

        };

        // update version

        if (typeof $localStorage.updated_version === 'undefined' && typeof $localStorage.userid !== 'undefined'){

            $http.post($rootScope.host + 'updateVersion', {user: $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                .then(function (data) {

                    if (data.data.status == '1')
                        $localStorage.updated_version = '1';

                }, function (err) {console.log(err);})

        }

        // get catalog categories

        $http.post($rootScope.host + 'GetDealCategoriesNew', '', {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
            .then(
            function (data) {

                for (var i = 0; i < data.data.length; i++) {

                    $rootScope.categories.push(data.data[i]);

                }

                var sales = {};

                for (var j = 0; j < $rootScope.categories.length; j++) {

                    if ($rootScope.categories[j].index == "10") {

                        sales = $rootScope.categories[j];

                        $rootScope.categories.splice(j, 1);
                        $rootScope.categories.unshift(sales);

                    }

                }

                console.log("Categories", $rootScope.categories);

            },

            function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});});


        // menu links

        $rootScope.categoryNumber = 0;
        $rootScope.categoryName = '';

        $rootScope.setCategory = function (x, y) {

            $rootScope.categoryNumber = x;
            $rootScope.categoryName = y;

            if (x != 0) {
                if (window.cordova) {
                    window.ga.trackEvent('קטגוריות', y);
                }
            }

        };

        // toggle menu

        $rootScope.toggleRightSideMenu = function () {

            $ionicSideMenuDelegate.toggleRight();

        };

        // get random questions

        $rootScope.getRandomQuestions = function (date, session) {

            $rootScope.divider = 20;
            $rootScope.questions = [];
            $rootScope.answers = [];
            $rootScope.rightAnswers = 0;
            $rootScope.session = null;

            $ionicLoading.show({template: 'טוען...'}).then(

                $http.post($rootScope.host + 'NewGetRandomQuestions', {

                    user : $localStorage.userid,
                    date: date,
                    session: session

                }, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                    .then(
                        function (data) {

                            $ionicLoading.hide();

                            $localStorage.once = 1;
                            $rootScope.once = $localStorage.once;
                            $localStorage.twice = 1;
                            $rootScope.twice = $localStorage.twice;
                            $rootScope.session = session;
                            $rootScope.questions = data.data;
                            console.log("Questions", $rootScope.questions);

                        }

                        , function(error){

                            $ionicLoading.hide();
                            $ionicPopup.alert({title: "אין חיבור לרשת"});

                        }))

        };

        // get all circles for the home page

        $rootScope.getUserMonth = function() {

            var deferred = $q.defer();

            $rootScope.days = [];
            $rootScope.firstNotAnswered = null;

            $http.post($rootScope.host + 'newGetMonth', {user: $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})

                .then(function(data){

                        $rootScope.correctAnswers = data.data.month_correct;
                        $rootScope.incorrectAnswers = data.data.month_incorrect;
                        $rootScope.monthPoints = data.data.points;
                        $rootScope.allPoints = data.data.all_points;

                        data.data.points = String(Number(data.data.points).toFixed(1));

                        if (data.data.points.length < 8){
                            $rootScope.points = Array(8 - data.data.points.length).join('0') + String(data.data.points);
                        } else {
                            $rootScope.points = data.data.points;
                        }

                        // $rootScope.prize1 = data.data.prize1;
                        // $rootScope.prize2 = data.data.prize2;
                        //
                        // console.log('Prizes', $rootScope.prize1, $rootScope.prize2);

                        for (var k = 0; k < data.data.month.length; k++){

                            if ((data.data.month[k].status === 'half' || data.data.month[k].status === 'empty')){

                                $rootScope.firstNotAnswered = data.data.month[k];
                                if ($rootScope.firstNotAnswered.status === 'empty')
                                    $rootScope.firstNotAnswered.session = '1';
                                else if ($rootScope.firstNotAnswered.status === 'half')
                                    $rootScope.firstNotAnswered.session = '2';

                                console.log('firstNotAnswered', $rootScope.firstNotAnswered);
                                break;
                            }

                        }

                        if (data.data.month.length < 26){

                            for (var l = data.data.month.length; l < 26; l++){

                                data.data.month.push({"mode": "usual", "status": "future", "number": l, "bonus_day": "0"})

                            }
                        }

                        $rootScope.days = data.data.month;
                        console.log("Days", $rootScope.days);

                        deferred.resolve();


                    },

                    function(error){

                    $ionicPopup.alert({title: "אין חיבור לרשת"});
                    deferred.reject();


                });

            return deferred.promise;
        };

        if (typeof $localStorage.userid !== 'undefined' && $localStorage.userid != '')
            $rootScope.getUserMonth();

        $interval(function() {$rootScope.setTimeToNext();}, 1000);

        $rootScope.setTimeToNext = function () {

            var noon = moment().hour(12).minute(0).second(0);
            var three = moment().hour(15).minute(0).second(0);
            var tomorrow = moment().add(1, 'day').hour(12).minute(0).second(0);
            var firstday = moment().isoWeekday("Sunday").hour(12).minute(0).second(0);

            if (moment().format('e') !== '6'){

                if (moment().isSameOrBefore(noon)){

                    var duration = moment.duration(noon.diff(moment()));
                    var hrs = duration.hours();
                    var mins = duration.minutes();

                    $rootScope.timeToNext = (hrs < 10 ? '0' + hrs : hrs) + ":" + (mins < 10 ? '0' + mins : mins);

                }
                else if (moment().isSameOrBefore(three)){

                    var duration = moment.duration(three.diff(moment()));
                    var hrs = duration.hours();
                    var mins = duration.minutes();

                    $rootScope.timeToNext = (hrs < 10 ? '0' + hrs : hrs) + ":" + (mins < 10 ? '0' + mins : mins);

                } else {

                    var duration = moment.duration(tomorrow.diff(moment()));
                    var hrs = duration.hours();
                    var mins = duration.minutes();

                    $rootScope.timeToNext = (hrs < 10 ? '0' + hrs : hrs) + ":" + (mins < 10 ? '0' + mins : mins);

                }

            } else {

                var duration = moment.duration(firstday.diff(moment()));
                var hrs = duration.hours();
                var mins = duration.minutes();

                $rootScope.timeToNext = (hrs < 10 ? '0' + hrs : hrs) + ":" + (mins < 10 ? '0' + mins : mins);

            }
        };


        // logout

        $rootScope.logout = function () {

            $http.post($rootScope.host + 'LogOutUser', {'user': $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                .then(
                function (data) {

                    console.log(data);

                    if (data.data[0].status == '1') {

                        $localStorage.email = "";
                        $localStorage.password = "";
                        $localStorage.userid = "";
                        $localStorage.soldier = "";
                        $localStorage.soldier_driver = "";
                        $localStorage.gender = "";
                        $localStorage.image = "";
                        $rootScope.days = [];
                        $rootScope.prize1 = '0';
                        $rootScope.prize2 = '0';

                        $state.go('app.login');


                    } else
                        $ionicPopup.alert({title: "אין חיבור לרשת"});

                },

                function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});});


        };

        // what time is it now?

        $rootScope.timeNow = ("0" + new Date().getHours()).slice(-2) + ":" + ("0" + new Date().getMinutes()).slice(-2);

        if ($rootScope.timeNow > "11:59") {

            $rootScope.today = ("0" + new Date().getDate()).slice(-2) + '/' + ("0" + (new Date().getMonth() + 1)).slice(-2) + '/' + new Date().getFullYear();
            $rootScope.todayDate = ("0" + new Date().getDate()).slice(-2) + '/' + ("0" + (new Date().getMonth() + 1)).slice(-2);

        } else {

            $rootScope.temp = new Date();
            $rootScope.temp = $rootScope.temp.setDate($rootScope.temp.getDate() - 1);

            $rootScope.today = ("0" + new Date($rootScope.temp).getDate()).slice(-2) + '/' + ("0" + (new Date($rootScope.temp).getMonth() + 1)).slice(-2) + '/' + new Date($rootScope.temp).getFullYear();
            $rootScope.todayDate = ("0" + new Date($rootScope.temp).getDate()).slice(-2) + '/' + ("0" + (new Date($rootScope.temp).getMonth() + 1)).slice(-2);

        }

        // get all deals without location

        $rootScope.getDealsWithoutLocation = function () {

            // alert("getDealsWithoutLocation");
            $http.post($rootScope.host + 'GetDeals', '', {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

            }).then(
                function (data) {

                    $rootScope.deals = data.data;

                    for (var i = 0; i < $rootScope.deals.length; i++) {

                        if ($rootScope.deals[i].linktitle == "") {

                            $rootScope.deals[i].linktitle = "קוד הטבה";

                        }

                        $rootScope.deals[i].imageSlider = [];

                        if ($rootScope.deals[i].image2 != "" || $rootScope.deals[i].image3 != "" || $rootScope.deals[i].image4 != "") {

                            $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image);

                            if ($rootScope.deals[i].image2 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image2);

                            }

                            if ($rootScope.deals[i].image3 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image3);

                            }

                            if ($rootScope.deals[i].image4 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image4);

                            }

                        }

                    }

                    $rootScope.isLocationEnabled = false;

                    console.log("DealsWithoutLocation", $rootScope.deals);

                },

                function (err) {

                    $ionicPopup.alert({
                        title: "אין חיבור לרשת",
                        buttons: [{
                            text: 'OK',
                            type: 'button-positive'
                        }]
                    });

                });

        };

        // get all deals with location

        $rootScope.getDealsWithLocation = function () {

            // alert("getDealsWithLocation");
            var send_coord = {'lat': $rootScope.lat, 'lng': $rootScope.lng};

            //GetDealsWithLocation2
            $http.post($rootScope.host + 'GetGrouponsWithLocations', send_coord, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

            }).then(
                function (data) {

                    $rootScope.deals = data.data;
                    $rootScope.closeDeals = [];

                    console.log("deals with location: " , $rootScope.deals);

                    for (var i = 0; i < $rootScope.deals.length; i++) {

                        if ($rootScope.deals[i].linktitle == "") {

                            $rootScope.deals[i].linktitle = "קוד הטבה";

                        }

                        $rootScope.deals[i].imageSlider = [];

                        if ($rootScope.deals[i].image2 != "" || $rootScope.deals[i].image3 != "" || $rootScope.deals[i].image4 != "") {

                            $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image);

                            if ($rootScope.deals[i].image2 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image2);

                            }

                            if ($rootScope.deals[i].image3 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image3);

                            }

                            if ($rootScope.deals[i].image4 != "") {

                                $rootScope.deals[i].imageSlider.push($rootScope.deals[i].image4);

                            }

                        }

                        for (var j = 0; j < $rootScope.deals[i].brances.length; j++) {

                            if (Number($rootScope.deals[i].brances[j][0].dist) <= 10) {

                                // console.log($rootScope.deals[i].brances[j][0]);
                                // var closestBranches = [];
                                // closestBranches.push($rootScope.deals[i].brances[j]);
                                // console.log(closestBranches);
                                //
                                // $rootScope.deals[i].brances = [];
                                //
                                // for (var k = 0; k < closestBranches.length; k++){
                                //
                                //     $rootScope.deals[i].brances.push(closestBranches[k]);
                                //
                                // }

                                $rootScope.closeDeals.push($rootScope.deals[i]);

                            }

                        }

                    }

                    $rootScope.isLocationEnabled = true;

                    console.log("closeDeals", $rootScope.closeDeals);

                    console.log("DealsWithLocation", $rootScope.deals);

                },

                function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});});

        };


        // working with transitions

        $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {

            if ((fromState.name === 'app.catalog' && toState.name === 'app.home') || (fromState.name === 'app.item' && toState.name === 'app.home')) {

                $rootScope.setCategory(0, "");

            }

            if (toState.name === 'app.month_prize'
                || toState.name === 'app.results'
                || (toState.name === 'app.test' && toParams.test_id === '0')
                || toState.name === 'app.test_results'){

                $ionicModal.fromTemplateUrl('templates/modal_ads.html', {scope: $rootScope, animation: 'slide-in-up'})

                    .then(function(modal) {

                        if (toState.name === 'app.month_prize'){

                            if(ionic.Platform.isIOS()){

                                DoubleClick.refreshAds('28lessonsPageIos');
                                DoubleClick.refreshAds('weekendPageIos');
                                DoubleClick.refreshAds('weekendIos');
                                DoubleClick.refreshAds('28lessonsIos');

                            } else if (ionic.Platform.isAndroid()){

                                DoubleClick.refreshAds('weekendPageAndroid');
                                DoubleClick.refreshAds('28lessonsPageAndroid');
                                DoubleClick.refreshAds('28lessonsAndroid');
                                DoubleClick.refreshAds('weekendAndroid');

                            }

                        } else if (toState.name === 'app.results') {

                            console.log('firstNotAnswered, stateChangeStart', $rootScope.firstNotAnswered);

                            if (ionic.Platform.isIOS()) {

                                DoubleClick.refreshAds('resultsPageIos');

                            } else if (ionic.Platform.isAndroid()) {

                                DoubleClick.refreshAds('resultsPageAndroid');

                            }

                        } else if (toState.name === 'app.test' && toParams.test_id === '0'){

                            if (ionic.Platform.isIOS()) {

                                DoubleClick.refreshAds('startTestPageIos');

                            } else if (ionic.Platform.isAndroid()) {

                                DoubleClick.refreshAds('startTestPageAndroid');

                            }

                        } else if (toState.name === 'app.test_results'){

                            if (ionic.Platform.isIOS()) {

                                DoubleClick.refreshAds('endTestPageIos');
                                DoubleClick.refreshAds('testResultsCubeIos');

                            } else if (ionic.Platform.isAndroid()) {

                                DoubleClick.refreshAds('endTestPageAndroid');
                                DoubleClick.refreshAds('testResultsCubeAndroid');

                            }

                        }

                        if ($rootScope.banners.length !== 0 && toState.name === 'app.results'){

                            if ($rootScope.banners.transition_results.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.transition_results.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        $rootScope.modal = modal;
                        $rootScope.modal.show()
                    });

            }

            if (fromState.name === 'app.home' && toState.name === 'app.catalog'){

                $ionicLoading.show({

                    template: 'טוען...'

                }).then(function () {

                    var send_data = {
                        'date' : $rootScope.today,
                        'soldier' : $localStorage.soldier,
                        'gender' : $localStorage.gender
                    };

                    $http.post($rootScope.host + 'GetDealByDate', send_data, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                        .then(
                            function (data) {

                                $ionicLoading.hide();

                                if (data.data.length != 0) {

                                    $rootScope.todayDeal = data.data[0];

                                    if ($rootScope.todayDeal.linktitle == "") {

                                        $rootScope.todayDeal.linktitle = "קוד הטבה";

                                    }

                                    $rootScope.todayDeal.imageSlider = [];

                                    if ($rootScope.todayDeal.image2 != "" || $rootScope.todayDeal.image3 != "" || $rootScope.todayDeal.image4 != "") {

                                        $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image);

                                        if ($rootScope.todayDeal.image2 != "") {

                                            $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image2);

                                        }

                                        if ($rootScope.todayDeal.image3 != "") {

                                            $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image3);

                                        }

                                        if ($rootScope.todayDeal.image4 != "") {

                                            $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image4);

                                        }

                                    }

                                    console.log('todayDeal', $rootScope.todayDeal);

                                    $rootScope.dailyDealPopup = $ionicPopup.show({
                                        templateUrl: 'templates/popup_daily_deal.html',
                                        scope: $rootScope,
                                        cssClass: 'prizesPopup'
                                    });

                                }

                            })

                }, function (err) {
                    $ionicLoading.hide();
                    $ionicPopup.alert({title: "אין חיבור לרשת"});
                })

            }

        });
        
        $rootScope.closeModal = function () {
            $rootScope.modal.hide();
            $rootScope.modal.remove();

            if ($rootScope.currState.current.name === 'app.results'){

                if ($rootScope.firstNotAnswered !== null) {

                    $rootScope.continuePopup = $ionicPopup.show({
                        templateUrl: 'templates/popup_continue.html',
                        scope: $rootScope,
                        cssClass: 'prizesPopup'
                    });
                }
            }
        };

        // banners

        $http.post($rootScope.host + 'NewGetBannersByType', '', {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
            .then(
            function (data) {

                $rootScope.banners = data.data;
                console.log("Banners", data.data);

            },

            function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});});

        $rootScope.sendShownBanner = function (banner_id) {

            $http.post($rootScope.host + 'addShownBanners', {user: $localStorage.userid, banner: banner_id}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})

        };

        $rootScope.getTestQuestions = function () {

            $rootScope.testQuestions = [];

            $ionicLoading.show({

                template: 'טוען...'

            }).then(function(){

                $http.get($rootScope.host + 'getTestQuestions').then(function (data) {

                    $ionicLoading.hide();
                    $rootScope.testQuestions = data.data;
                    console.log('testQuestions', data.data);

                    $state.go('app.test', {test_id: 0});

                }, function (err) {

                    $ionicLoading.hide();
                    $ionicPopup.alert({title: "אין חיבור לרשת"});

                })

            })

        }
    })

    .config(function ($stateProvider, $urlRouterProvider,DoubleClickProvider) {
		
  DoubleClickProvider
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'androidInformation')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'iosInformation')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], '28lessonsAndroid')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], '28lessonsIos')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'weekendAndroid')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'weekendIos')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'received500Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'received500Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'received1000Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'received1000Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'inter1Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'inter1Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'inter2Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'inter2Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'inter3Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'inter3Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'inter4Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'inter4Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'inter5Android')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'inter5Ios')
      .defineSlot('/48282107/AndroidStickyInfoPage320//50', [320, 50], 'personalAndroid')
      .defineSlot('/48282107/IOSStickyInfoPage320//50', [320, 50], 'personalIos')

      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'testResultsCubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'testResultsCubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'received500CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'received500CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'received1000CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'received1000CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'inter1CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'inter1CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'inter2CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'inter2CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'inter3CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'inter3CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'inter4CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'inter4CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'inter5CubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'inter5CubeIos')
      .defineSlot('/48282107/ANDROID300//250CUBE', [300, 250], 'resultsCubeAndroid')
      .defineSlot('/48282107/IOS300//250CUBE', [300, 250], 'resultsCubeIos')

      .defineSlot('/48282107/AndroidAdmobInterstitial', [320, 480], '28lessonsPageAndroid')
      .defineSlot('/48282107/IosAdmobInterstitial', [320, 480], '28lessonsPageIos')
      .defineSlot('/48282107/AndroidAdmobInterstitial', [320, 480], 'weekendPageAndroid')
      .defineSlot('/48282107/IosAdmobInterstitial', [320, 480], 'weekendPageIos')
      .defineSlot('/48282107/AndroidAdmobInterstitial', [320, 480], 'resultsPageAndroid')
      .defineSlot('/48282107/IosAdmobInterstitial', [320, 480], 'resultsPageIos')
      .defineSlot('/48282107/AndroidAdmobInterstitial', [320, 480], 'startTestPageAndroid')
      .defineSlot('/48282107/IosAdmobInterstitial', [320, 480], 'startTestPageIos')
      .defineSlot('/48282107/AndroidAdmobInterstitial', [320, 480], 'endTestPageAndroid')
      .defineSlot('/48282107/IosAdmobInterstitial', [320, 480], 'endTestPageIos')
  ;
					 
        $stateProvider

            .state('app', {
                url: '/app',
                abstract: true,
                templateUrl: 'templates/menu.html',
                controller: 'AppCtrl'
            })

            .state('app.router', {
                url: '/router',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/router.html',
                        controller: 'RouterCtrl'
                    }
                }

            })

            .state('app.enter', {
                url: '/enter',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/enter.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('app.register', {
                url: '/register',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/register.html',
                        controller: 'RegisterCtrl'
                    }
                }
            })

            .state('app.login', {
                url: '/login',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/login.html',
                        controller: 'LoginCtrl'

                    }
                }
            })

            .state('app.home', {
                url: '/home',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/home.html',
                        controller: 'HomeCtrl'
                    }
                }
            })

            .state('app.catalog', {
                url: '/catalog',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/catalog.html',
                        controller: 'CatalogCtrl'
                    }
                }
            })

            .state('app.item', {
                url: '/item/:itemId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/item.html',
                        controller: 'ItemCtrl'
                    }
                }
            })

            .state('app.discount', {
                url: '/discount',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/discount.html',
                        controller: 'DiscountCtrl'
                    }
                }
            })

            .state('app.personal', {
                url: '/personal',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/personal.html',
                        controller: 'PersonalCtrl'
                    }
                }
            })

            .state('app.question_to_specialist', {
                url: '/question_to_specialist',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/question_to_specialist.html',
                        controller: 'PersonalCtrl'
                    }
                }
            })

            .state('app.information', {
                url: '/information',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/information.html',
                        controller: 'InformationCtrl'
                    }
                }
            })

            .state('app.leaderboard', {
                url: '/leaderboard',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/leaderboard.html',
                        controller: 'LeaderboardCtrl'
                    }
                }
            })

            .state('app.article', {
                url: '/article/:articleId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/article.html',
                        controller: 'ArticleCtrl'
                    }
                }
            })

            .state('app.inter', {
                url: '/inter/:date/:questionId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/inter.html',
                        controller: 'QuestionsCtrl'
                    }
                }
            })

            .state('app.questions', {
                url: '/questions/:date/:questionId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/questions.html',
                        controller: 'QuestionsCtrl'
                    }
                }
            })

            .state('app.answers', {
                url: '/answers/:date/:questionId',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/answers.html',
                        controller: 'QuestionsCtrl'
                    }
                }
            })

            .state('app.results', {
                url: '/results',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/results.html',
                        controller: 'QuestionsCtrl'
                    }
                }
            })

            // .state('app.shabbat', {
            //     url: '/shabbat',
            //     views: {
            //         'menuContent': {
            //             templateUrl: 'templates/shabbat.html'
            //         }
            //     }
            // })


            .state('app.daily_questions', {
                url: '/daily_questions',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/daily_questions.html'
                    }
                }
            })

            // .state('app.birthday', {
            //     url: '/birthday',
            //     views: {
            //         'menuContent': {
            //             templateUrl: 'templates/birthday.html'
            //         }
            //     }
            // })

            .state('app.terms', {
                url: '/terms',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/terms.html'
                    }
                }
            })

            .state('app.month_prize', {
                url: '/month_prize',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/month_prize.html',
                        controller: 'MonthPrizeCtrl'
                    }
                }
            })

            .state('app.enter_test', {
                url: '/enter_test',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/enter_test.html'
                    }
                }
            })

            .state('app.test', {
                url: '/test/:test_id',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/test.html',
                        controller: 'TestCtrl'
                    }
                }
            })

            .state('app.test_results', {
                url: '/test_results',
                views: {
                    'menuContent': {
                        templateUrl: 'templates/test_results.html'
                    }
                }
            })
        ;
        // if none of the above states are matched, use this as the fallback router
        $urlRouterProvider.otherwise('/app/router');
    });
