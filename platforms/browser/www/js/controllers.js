angular.module('starter.controllers', [])

    .controller('AppCtrl', function ($scope, $localStorage, $ionicModal, $timeout, $state) {

        // With the new view caching in Ionic, Controllers are only called
        // when they are recreated or on app start, instead of every page change.
        // To listen for when this page is active (for example, to refresh data),
        // listen for the $ionicView.enter event:
        //$scope.$on('$ionicView.enter', function(e) {
        //});


    })

    .controller('RouterCtrl', function($scope, $ionicPopup, $http, $rootScope, $localStorage, $state, $ionicHistory){

        if (typeof $localStorage.enterScreenIsSeen != "undefined"){

            if ($localStorage.password && $localStorage.password != "") {

                $http.post($rootScope.host + 'CheckUserSeenDeal', {"user" : $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                    .then(

                    function(data){

                        if (data.data.response.seendeal == "0"){

                            $localStorage.isDailyDealSeen = false;
                            $rootScope.isDailyDealSeen = $localStorage.isDailyDealSeen;

                            $rootScope.getUserMonth();

                            $ionicHistory.nextViewOptions({
                                disableAnimate: true,
                                expire: 300
                            });

                            $state.go('app.home');

                        } else {

                            $localStorage.isDailyDealSeen = true;
                            $rootScope.isDailyDealSeen = $localStorage.isDailyDealSeen;

                            $rootScope.getUserMonth();

                            $ionicHistory.nextViewOptions({
                                disableAnimate: true,
                                expire: 300
                            });

                            $state.go('app.home');

                        }

                    },

                    function(err){$ionicPopup.alert({title: "אין חיבור לרשת"});});

            } else {

                $ionicHistory.nextViewOptions({
                    disableAnimate: true,
                    expire: 300
                });

                $state.go('app.login');

            }

        } else {

            $ionicHistory.nextViewOptions({
                disableAnimate: true,
                expire: 300
            });

            $state.go('app.enter');
            $localStorage.enterScreenIsSeen = true;

        }

    })

    .controller('RegisterCtrl', function ($ionicSideMenuDelegate, $q, $ionicLoading, $scope, $ionicPopup, $http, $rootScope, $localStorage, $state) {

        $scope.goToRegister = function(){

            $state.go("app.register");

            if(window.cordova)
                window.ga.trackEvent('מסך הסלידר נצפה', 'כן');

        };

        $scope.$on('$ionicView.enter', function(e) {

            if(window.cordova)
                window.ga.trackView($state.current.name);

        });

        $ionicSideMenuDelegate.canDragContent(false);

        // Facebook login - works, don't touch it!

        // This is the success callback from the login method
        var fbLoginSuccess = function(response) {
            //alert(JSON.stringify(response));
            console.log(response);
            if (!response.authResponse){
                fbLoginError("Cannot find the authResponse");
                return;
            }

            var authResponse = response.authResponse;

            getFacebookProfileInfo(authResponse)
                .then(function(profileInfo) {

                    $scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);

                    $ionicLoading.hide();
                    //$state.go('app.home');
                }, function(fail){
                    // Fail get profile info
                    console.log('profile info fail', fail);
                });
        };

        // This is the fail callback from the login method
        var fbLoginError = function(error){
            //alert(JSON.stringify(error));
            console.log('fbLoginError', error);
            $ionicLoading.hide();
        };

        // This method is to get the user profile info from the facebook api
        var getFacebookProfileInfo = function (authResponse) {
            var info = $q.defer();

            facebookConnectPlugin.api('/me?fields=email,name,gender,first_name,last_name,locale&access_token=' + authResponse.accessToken, null,
                function (response) {
                    //alert(JSON.stringify(response));
                    console.log(response);
                    info.resolve(response);
                },
                function (response) {
                    //alert(JSON.stringify(response));
                    console.log(response);
                    info.reject(response);
                }
            );
            return info.promise;
        };

        //This method is executed when the user press the "Login with facebook" button
        $scope.FaceBookLoginBtn = function() {

            facebookConnectPlugin.getLoginStatus(function(success){
                //alert(JSON.stringify(success));
                if(success.status === 'connected'){
                    // The user is logged in and has authenticated your app, and response.authResponse supplies
                    // the user's ID, a valid access token, a signed request, and the time the access token
                    // and signed request each expire
                    console.log('getLoginStatus', success.status);

                    getFacebookProfileInfo(success.authResponse)
                        .then(function(profileInfo) {

                            $scope.FacebookLoginFunction(profileInfo.id,profileInfo.first_name,profileInfo.last_name,profileInfo.email,profileInfo.gender);

                        }, function(fail){
                            // Fail get profile info
                            console.log('profile info fail', fail);
                        });

                } else {
                    // If (success.status === 'not_authorized') the user is logged in to Facebook,
                    // but has not authenticated your app
                    // Else the person is not logged into Facebook,
                    // so we're not sure if they are logged into this app or not.

                    //console.log('getLoginStatus', success.status);

                    $ionicLoading.show({
                        template: 'loading...<ion-spinner icon="spiral"></ion-spinner>'
                    });


                    // Ask the permissions you need. You can learn more about
                    // FB permissions here: https://developers.facebook.com/docs/facebook-login/permissions/v2.4
                    facebookConnectPlugin.login(['email', 'public_profile'], fbLoginSuccess, fbLoginError);
                }
            });
        };

        $scope.FacebookLoginFunction = function(id,firstname,lastname,email,gender)
        {
            $scope.fullname = firstname+' '+lastname;
            $scope.gender = (gender == "male" ? "0" : "1");

            $scope.register.firstname = firstname;
            $scope.register.lastname = lastname;
            $scope.register.email = email;
            $scope.register.gender = $scope.gender;
        };

        // google autocomplete options

        $scope.autocompleteOptions = {componentRestrictions: { country: 'il' }};
        $scope.termsAccepted = false;
        $scope.selectGender = function (x) {$scope.register.gender = x;};
        $scope.selectSoldier = function (x) {$scope.register.soldier = x;};
        $scope.selectSoldierDriver = function (x) {$scope.register.soldier_driver = x;};
        $scope.acceptTerms = function () {$scope.termsAccepted = !$scope.termsAccepted};

        // registration

        $scope.register = {

            'firstname' : "",
            'lastname' : "",
            'email' : "",
            'password' : "",
            'gender' : "",
            'birthday' : "",
            'maps' : "",
            'soldier' : "",
            'soldier_driver' : "",
            'gift' : "",
            'code' : ""

        };

        $scope.makeRegistration = function(){

            //alert(JSON.stringify($scope.register));
            // console.log($scope.register);

            var emailRegex = /\S+@\S+\.\S+/;

            if ($scope.register.firstname == "" || $scope.register.lastname == "" ||$scope.register.email == "" ||
                $scope.register.password == "" || $scope.register.gender == "" || $scope.register.birthday == ""
                || $scope.register.maps == ""  || $scope.register.soldier == "" || $scope.register.gift == ""){

                $ionicPopup.alert({title: "נא למלא את כל השדות"});

            } else if ($scope.register.password.length < 3){

                $ionicPopup.alert({title: "דרוש יותר משלושה תווים לסיסמא"});

            } else if (!emailRegex.test($scope.register.email)){

                $ionicPopup.alert({title: "נא להכניס כתובת מייל תקינה"});

            } else if ($scope.register.soldier == "1" && $scope.register.soldier_driver == ""){

                $ionicPopup.alert({title: "נא למלא soldier_driver"});

            }  else if ($scope.register.soldier == "0" && $scope.register.soldier_driver !== ""){

                $scope.register.soldier_driver = ""

            }  else if ($scope.termsAccepted === false){

                $ionicPopup.alert({title: "Please accept Terms and Conditions"});

            } else {

                //console.log($scope.register);

                var send_data = {

                    'firstname' : $scope.register.firstname,
                    'lastname' : $scope.register.lastname,
                    'email' : $scope.register.email,
                    'password' : $scope.register.password,
                    'gender' : $scope.register.gender,
                    'birthday' : $scope.register.birthday,
                    'address' : $scope.register.maps.formatted_address,
                    'location_lat' : $scope.register.maps.geometry.location.lat(),
                    'location_lng' : $scope.register.maps.geometry.location.lng(),
                    'soldier' : $scope.register.soldier,
                    'soldier_driver' : $scope.register.soldier_driver,
                    'gift' : $scope.register.gift,
                    'code' : $scope.register.code,
                    'push_id' : $rootScope.pushId

                };

                //alert(JSON.stringify(send_data));

                $http.post($rootScope.host + 'NewRegisterUser', send_data, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                    .then(

                    function(data){
                       // alert(JSON.stringify(data.data));
                        console.log(data);

                        if (data.data.response.status == "0"){

                            $localStorage.userid = data.data.response.userid;
                            $localStorage.email = $scope.register.email;
                            $localStorage.password = $scope.register.password;
                            $localStorage.firstname = $scope.register.firstname;
                            $localStorage.lastname = $scope.register.lastname;
                            $localStorage.gender = $scope.register.gender;
                            $localStorage.birthday = $scope.register.birthday;
                            $localStorage.soldier = $scope.register.soldier;
                            $localStorage.soldier_driver = $scope.register.soldier_driver;
                            $localStorage.gift = $scope.register.gift;

                            $rootScope.userData.userid = $localStorage.userid;
                            $rootScope.userData.email = $localStorage.email;
                            $rootScope.userData.password = $localStorage.password;
                            $rootScope.userData.firstname = $localStorage.firstname;
                            $rootScope.userData.lastname = $localStorage.lastname;
                            $rootScope.userData.gender = $localStorage.gender;
                            $rootScope.userData.birthday = $localStorage.birthday;
                            $rootScope.userData.soldier = $localStorage.soldier;
                            $rootScope.userData.soldier_driver = $localStorage.soldier_driver;
                            $rootScope.userData.gift = $localStorage.gift;

                            $rootScope.getUserMonth();
                            $state.go('app.home');

                        } else {

                            $ionicPopup.alert({title: "המשתמש כבר רשום במערכת"}).then(function(){$state.go('app.login')});

                        }

                    },

                    function(error){$ionicPopup.alert({title: "אין חיבור לרשת"})}
                );

            }

        }

    })


    .controller('LoginCtrl', function ($ionicLoading, $ionicSideMenuDelegate, $scope, $ionicPopup, $ionicModal, $http, $rootScope, $localStorage, $state) {

        $ionicSideMenuDelegate.canDragContent(false);

        $scope.$on('$ionicView.enter', function(e) {

            // if(window.cordova)
            //     window.ga.trackView("עמוד כניסה");

            $scope.isSent = false;
            $scope.login = {'email' : $localStorage.email, 'password' : ''};

            // login

            $scope.makeLogin = function(){

                var emailRegex = /\S+@\S+\.\S+/;

                if($scope.login.email == '' || $scope.login.password == '') {

                    $ionicPopup.alert({title: "נא למלא את כל השדות"});

                } else {

                    var send_data = {

                        'email' : $scope.login.email,
                        'password' : $scope.login.password,
                        'push_id' : $rootScope.pushId

                    };

                    // alert(JSON.stringify(send_data));

                    $http.post($rootScope.host + 'NewLoginUser', send_data, {

                        headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

                    }).then(

                        function(data){

                            console.log(data);

                            if (data.data.response.status == "0"){

                                $ionicPopup.alert({
                                    title: 'טעות במייל או בסיסמא. נא ללחוץ על הכתפור "שכחתי סיסמא" במידת הצורך',
                                    buttons: [{
                                        text: 'OK',
                                        type: 'button-positive'
                                    }]
                                })

                            } else {

                                if (typeof $localStorage.updated_version === 'undefined' && typeof $localStorage.userid !== 'undefined'){

                                    $http.post($rootScope.host + 'updateVersion', {user: $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                                        .then(function (data) {

                                            if (data.status == '1')
                                                $localStorage.updated_version = '1';

                                        }, function (err) {console.log(err);})

                                }

                                $localStorage.firstname = data.data.response.firstname;
                                $localStorage.lastname = data.data.response.lastname;
                                $localStorage.email = $scope.login.email;
                                $localStorage.password = $scope.login.password;
                                $localStorage.birthday = data.data.response.birthday;
                                $localStorage.userid = data.data.response.userid;
                                $localStorage.soldier = data.data.response.soldier;
                                $localStorage.soldier_driver = data.data.response.soldier_driver;
                                $localStorage.gift = data.data.response.gift;
                                $localStorage.gender = data.data.response.gender;
                                $localStorage.image = data.data.response.image;

                                $rootScope.userData = data.data.response;
                                $rootScope.userData.password = $scope.login.password;
                                $rootScope.image = $localStorage.image;

                                $rootScope.getUserMonth();

                                $state.go('app.home');

                                $scope.login = {

                                    'email' : $localStorage.email,
                                    'password' : '',
                                    'push_id' : $rootScope.pushId

                                };

                            }

                        },

                        function(error){$ionicPopup.alert({title: "אין חיבור לרשת"})}
                    );

                }

            };

        });
        // forgot password

        $scope.openForgotPasswordModal = function () {

            $ionicModal.fromTemplateUrl('templates/modal_forgot_password.html', {scope: $scope})

                .then(function (ForgotPasswordModal) {

                $scope.ForgotPasswordModal = ForgotPasswordModal;
                $scope.ForgotPasswordModal.show();

            });
        };

        $scope.closeForgotPasswordModal = function () {$scope.ForgotPasswordModal.hide();};

        $scope.forgot = {'email' : ''};
        $scope.isSent = false;

        $scope.sendForgotPassword = function(){

            $ionicLoading.show({

                template: 'טוען...'

            }).then(function(){

                $http.post($rootScope.host + 'ForgotPassword', $scope.forgot, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                    .then(

                    function(data){

                        console.log(data.data);

                        if (data.data[0].status == "1"){

                            $ionicLoading.hide();
                            $scope.isSent = true;

                        } else {

                            $ionicLoading.hide();

                            $ionicPopup.alert({title: "טעות בכתובת המייל - נא לנסות שנית"})

                        }

                    },

                    function(error){

                        $ionicLoading.hide();
                        $ionicPopup.alert({title: "אין חיבור לרשת"})

                    });

            });

        };

    })

    .controller('HomeCtrl', function ($ionicScrollDelegate, $interval, $ionicLoading, $ionicSideMenuDelegate, $scope, $rootScope, $localStorage, $state, $http, $ionicPopup) {

        $ionicScrollDelegate.freezeScroll(true);

        $scope.$on('$ionicView.enter', function(e) {

            if(window.cordova)
                window.ga.trackView("עמוד הבית");

            $rootScope.setTimeToNext();

        });

        $ionicSideMenuDelegate.canDragContent(false);
		$scope.ShowMainBanners = 0;
        $scope.options = {loop: true, effect: 'slide', speed: 300, autoplay: 3000, pagination: false};
        $scope.isDayTime = moment().isSameOrBefore(moment().hour(18).minute(0).second(0));
        // $scope.prizePopupImg = '';
        //
        // $scope.showPresent = showPresent;
        //
        // function showPresent(x) {
        //
        //     $scope.prizePopupImg = x === 1 ? 'img/popups/popup_500.png' : 'img/popups/popup_1000.png';
        //
        //     $scope.prizePopup = $ionicPopup.show({
        //         templateUrl: 'templates/popup_prizes.html',
        //         scope: $scope,
        //         cssClass: 'prizesPopup'
        //     });
        //
        // }

    })

    .controller('DiscountCtrl', function (isFavoriteFactory, makeFavoriteFactory, $sce, $localStorage, $scope, $rootScope, $state, $http, $ionicPopup) {

        $scope.options = {loop: true, effect: 'slide', speed: 300, autoplay: 3000, pagination: false};

        $scope.$on('$ionicView.enter', function(e) {

            if(window.cordova)
                window.ga.trackView("עמוד דיל יומי");

            var send_data = {
                'date' : $rootScope.today,
                'soldier' : $localStorage.soldier,
                'gender' : $localStorage.gender
            };

            console.log(send_data);

            // get deal for today

            $scope.noTodayDeal = false;

            $http.post($rootScope.host + 'GetDealByDate', send_data, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

            }).then(
                function (data) {

                    if (data.data.length == 0){

                        $scope.noTodayDeal = true;

                    } else {

                        $scope.noTodayDeal = false;

                        $rootScope.todayDeal = data.data[0];

                        if ($rootScope.todayDeal.linktitle == ""){

                            $rootScope.todayDeal.linktitle = "קוד הטבה" ;

                        }

                        $rootScope.todayDeal.imageSlider = [];

                        if ($rootScope.todayDeal.image2 != "" || $rootScope.todayDeal.image3 != "" || $rootScope.todayDeal.image4 != ""){

                            $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image);

                            if ($rootScope.todayDeal.image2 != "") {

                                $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image2);

                            }

                            if ($rootScope.todayDeal.image3 != "") {

                                $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image3);

                            }

                            if ($rootScope.todayDeal.image4 != "") {

                                $rootScope.todayDeal.imageSlider.push($rootScope.todayDeal.image4);

                            }

                        }

                        if ($rootScope.todayDeal.showiframe == "1"){

                            $scope.iframeLink = $sce.trustAsResourceUrl($rootScope.todayDeal.codelink);

                        }

                        console.log("Today Deal", $rootScope.todayDeal);

                        if ($state.current.name == "app.discount"){

                            var data_send = {

                                "user" : $localStorage.userid,
                                "deal_id" : $rootScope.todayDeal.index,
                                "supplier_id" : $rootScope.todayDeal.supplier_id

                            };

                            $http.post($rootScope.host + 'CountDealView', data_send, {

                                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

                            }).then(

                                function(data){

                                    console.log(data);
                                },

                                function(err){$ionicPopup.alert({title: "אין חיבור לרשת"});});

                        }

                    }

                },

                function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});});

        });

        // check if the deal is favorite

        $scope.isFavorite = function (x) {return isFavoriteFactory.isFavorite(x);};

        // make favorite

        $scope.makeFavorite = function(x, y){return makeFavoriteFactory.makeFavorite(x, $scope, y);};

        // open links

        $scope.goToLink = function(x, y){

            cordova.InAppBrowser.open(x, '_blank', 'location=yes');
            if(window.cordova) {
                window.ga.trackEvent('לינק בהטבה היומית', y);
            }

        };

    })

    .controller('PersonalCtrl', function ($ionicModal, DoubleClick, dateFilter, $timeout, $ionicScrollDelegate, $ionicSideMenuDelegate, $http, $scope, $rootScope, $ionicPopup, $localStorage, $cordovaCamera, $state) {

        $scope.$on('$ionicView.enter', function(e) {

            if(ionic.Platform.isIOS()){

                DoubleClick.refreshAds('personalIos');

            }  else if (ionic.Platform.isAndroid()){

                DoubleClick.refreshAds('personalAndroid');

            }

            if ($rootScope.banners.length !== 0){

                if ($rootScope.banners.personal.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.personal.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }
            }

            if(window.cordova){
                if ($state.current.name == "app.personal"){
                    window.ga.trackView("עמוד אזור אישי");
                } else {
                    window.ga.trackView("עמוד פניה למומחה");
                }
            }

            // for push notifications from specialist answers

            if ($rootScope.pushNotificationType) {

                if ($rootScope.pushNotificationType == "newmessage"){

                    $scope.setSelection('message');
                    $rootScope.pushNotificationType = "";

                }

            }

            // for scroll at app.question_to_specialist

            if ($state.current.name == "app.question_to_specialist") {

                $timeout(function(){

                    $ionicScrollDelegate.$getByHandle('smallScroll').scrollBottom(true);

                }, 100)

            }

            $http.post($rootScope.host + 'getUser', {user: $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})

                .then(function (data) {

                       $scope.personalInformation.address = data.data.address == null ? '' : data.data.address;

                    },

                    function (error) {$ionicPopup.alert({title: "אין חיבור לרשת"})})

        });

        $ionicSideMenuDelegate.canDragContent(false);

        // select tab

        $scope.selection = 'personal';

        $scope.setSelection = function(x){

            $scope.selection = x;

            if ($scope.selection === 'message')
                $timeout(function(){$ionicScrollDelegate.$getByHandle('smallScroll').scrollBottom(true);}, 100);

        };

        // PROFILE

        // fill in personal info

        $scope.personalInformation = {

            "firstname" : $localStorage.firstname,
            "lastname" : $localStorage.lastname,
            "soldier" : Number($localStorage.soldier),
            "soldier_driver" : Number($localStorage.soldier_driver),
            "gift" : String($localStorage.gift),
            "image" : $localStorage.image,
            'address' : '',
            "old_password" : "",
            "new_password" : ""

        };

        $scope.selectSoldier = function (x) {$scope.personalInformation.soldier = x;};
        $scope.selectSoldierDriver = function (x) {$scope.personalInformation.soldier_driver = x;};

        // working with picture

        $scope.userpic = '';
        $scope.userpicURL = '';

        $scope.getPhoto = function () {

            var options = {

                quality : 75,
                destinationType : Camera.DestinationType.FILE_URI,  //Camera.DestinationType.DATA_URL,
                sourceType : Camera.PictureSourceType.PHOTOLIBRARY,
                allowEdit : true,
                encodingType: Camera.EncodingType.JPEG,
                targetWidth: 600,
                targetHeight: 600,
                popoverOptions: CameraPopoverOptions,
                saveToPhotoAlbum: false,
                correctOrientation: true

            };

            $cordovaCamera.getPicture(options).then(

                function(data){

                    $scope.userpic = data;

                    // alert($scope.userpic);

                    var options = new FileUploadOptions();

                    options.mimeType = "jpeg";
                    options.fileKey = "file";
                    options.chunkedMode = false;

                    // options.fileName = $scope.userpic.substr(($scope.userpic.lastIndexOf("/")+1), $scope.userpic.indexOf("?"));
                    // alert(options.fileName);

                    var ft = new FileTransfer();

                    ft.upload($scope.userpic, encodeURI($rootScope.host + "UploadNewImage"), function(data){

                        // alert(JSON.stringify(data.response));
                        $scope.userpicURL = $rootScope.phpHost + data.response;
                        $localStorage.image = $scope.userpicURL;
                        $rootScope.image = $localStorage.image;

                    }, function(err){$ionicPopup.alert({title: "התמונה לא נטענה"});}, options);

                },

                function(err) {$ionicPopup.alert({title: "התמונה לא נטענה"});}
            )

        };

        // save information and picture

        $scope.saveChanges = function(){

            console.log($scope.personalInformation);

            // 1. check everything

            var emailRegex = /\S+@\S+\.\S+/;

            if ($scope.personalInformation.firstname == "" || $scope.personalInformation.lastname == "" || $scope.personalInformation.soldier == null){

                $ionicPopup.alert({title: "נא למלא את כל השדות"});

            } else if ($scope.personalInformation.soldier == 1 && $scope.personalInformation.soldier_driver == null) {

                $ionicPopup.alert({title: "נא למלא את כל השדות"});

            } else if ($scope.personalInformation.old_password != "" && $scope.personalInformation.old_password != $localStorage.password) {

                $ionicPopup.alert({title: "נא להזין סיסמא נוכחית"});

            } else if ($scope.personalInformation.old_password != "" && $scope.personalInformation.new_password == ""){

                $ionicPopup.alert({title: "נא להזין סיסמא חדשה"});

            } else if ($scope.personalInformation.new_password != "" && $scope.personalInformation.new_password.length < 3){

                $ionicPopup.alert({title: "דרוש יותר משלושה תווים לסיסמא"});

            } else {

                    // 3. on success - collect everything in one variable

                    var send_data = {

                            "user" : $localStorage.userid,
                            "firstname" : $scope.personalInformation.firstname,
                            "lastname" : $scope.personalInformation.lastname,
                            'address' : '',
                            "soldier" : $scope.personalInformation.soldier,
                            "soldier_driver" : $scope.personalInformation.soldier_driver,
                            "giftselected" : $scope.personalInformation.gift,
                            "image" : $localStorage.image,
                            "password" : ''

                    };

                    if ($scope.personalInformation.new_password != ""){

                        send_data.password = $scope.personalInformation.new_password;

                    } else {

                        send_data.password = $localStorage.password;

                    }

                    if ($localStorage.soldier != $scope.personalInformation.soldier){

                        send_data.soldier == $scope.personalInformation.soldier;

                    }

                    if ($localStorage.soldier == '1' && $scope.personalInformation.soldier == '0'){

                        send_data.soldier_driver = 0;

                    }

                    if (angular.isObject($scope.personalInformation.address)){

                        send_data.address = $scope.personalInformation.address.formatted_address;

                    } else {

                        send_data.address = $scope.personalInformation.address;

                    }


                    // alert(JSON.stringify(send_data));
                    console.log(send_data);

                    // 3. send update info

                    $http.post($rootScope.host + 'updateProfileNew', send_data, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                        .then(

                        function(data){

                            console.log(data);

                           if (data.data[0].status == '1'){

                               // 4. if success, update localStorage and rootScope

                               $localStorage.firstname = send_data.firstname;
                               $localStorage.lastname = send_data.lastname;
                               $localStorage.password = send_data.password;
                               $localStorage.soldier = send_data.soldier;
                               $localStorage.soldier_driver = send_data.soldier_driver;
                               $localStorage.gift = send_data.giftselected;

                               if ($scope.personalInformation.new_password != ""){

                                   $localStorage.password = send_data.password;

                               }

                               $rootScope.userData = {
                                   "firstname" : $localStorage.firstname,
                                   "lastname" : $localStorage.lastname,
                                   "password" : $localStorage.password,
                                   "email" : $localStorage.email,
                                   "birthday" : $localStorage.birthday,
                                   "gender" : $localStorage.gender,
                                   "soldier" : $localStorage.soldier,
                                   "soldier_driver" : $localStorage.soldier_driver,
                                   "gift" : $localStorage.gift,
                                   "userid" : $localStorage.userid
                               };

                               $scope.personalInformation = {

                                   "firstname" : $localStorage.firstname,
                                   "lastname" : $localStorage.lastname,
                                   'address' : send_data.address,
                                   "soldier" : $localStorage.soldier,
                                   "soldier_driver" : $localStorage.soldier_driver,
                                   "gift" : $localStorage.gift,
                                   "old_password" : "",
                                   "new_password" : ""

                               };

                               // 5. inform the user that everything is ok

                               var updatedInfoPopup = $ionicPopup.show({
                                   templateUrl: 'templates/popup_updated_info.html',
                                   scope: $scope,
                                   cssClass: 'updatedInfoPopup'
                               });

                               $rootScope.hideUpdatedInfoPopup = function () {

                                   updatedInfoPopup.close();

                               };

                           } else {

                               $ionicPopup.alert({title: "המידע לא עודכן"})

                           }

                        },

                        function(error){$ionicPopup.alert({title: "אין חיבור לרשת"})}
                    );

            }

        };

        // MESSAGE TO A SPECIALIST

        // get all questions/answers

        $scope.messages = [];

        $http.post($rootScope.host + 'GetSpecialistMsg', {"user" : $localStorage.userid}, {

                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

            }).then(

                function(data){

                    $scope.messages = data.data;

                    for(var i = 0; i < $scope.messages.length; i++){

                        $scope.messages[i].date = Date.parse($scope.messages[i].date);

                    }

                    console.log("Messages", $scope.messages);

                },

                function(err){$ionicPopup.alert({title: "אין חיבור לרשת"})});

        // send message

        $scope.message = {"subject" : ""};

        $scope.sendMessage = function(){

            if ($scope.message.subject == ""){

                $ionicPopup.alert({title: "נא לכתוב את שאלתך למומחה"})

            } else {

                var send_message = {

                    "user" : $localStorage.userid,
                    "message" : $scope.message.subject

                };

                console.log(send_message);

                $http.post($rootScope.host + 'MessageSpecialist', send_message, {

                    headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}

                }).then(

                    function(data){

                        if (data.data[0].status == "1"){

                            $ionicPopup.alert({title: "פנייתך תועבר למומחה, תשובה בימים הקרובים"});

                            var new_message = {
                                "date" : new Date(),
                                "index" : "",
                                "message" : $scope.message.subject,
                                "type" : "question",
                                "userid" : $localStorage.userid
                            };

                            $scope.messages.push(new_message);
                            $scope.message = {"subject" : ""};

                        } else {

                            $ionicPopup.alert({title: "אין חיבור לרשת"});

                        }

                    },

                    function(err){

                        $ionicPopup.alert({title: "אין חיבור לרשת"});

                    });

            }

        }

    })

    .controller('CatalogCtrl', function ($ionicPlatform, $ionicScrollDelegate, $ionicLoading, $cordovaGeolocation, isFavoriteFactory, deleteFavoriteFactory, makeFavoriteFactory, $scope, $rootScope, $http, $ionicPopup, $state, $localStorage) {

        $scope.$on('$ionicView.enter', function(e) {

            if(window.cordova){
                window.ga.trackView("קטלוג");
            }

        });

        // scroll to top

        $scope.scrollTop = function () {

            $ionicScrollDelegate.scrollTop('shouldAnimate');

        };

        // open page at catalog

        $scope.openItem = function(x){

            var data_send = {

                "user" : $localStorage.userid,
                "deal_id" : x.index,
                "supplier_id" : x.supplier_id

            };

            $http.post($rootScope.host + 'CountDealView', data_send, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                .then(

                function(data){console.log(data);},

                function(err){$ionicPopup.alert({title: "אין חיבור לרשת"});});

            if (x.showiframe == "0" && x.dealgivenby == "1"){

                $state.go('app.item', {itemId:x.index});
                cordova.InAppBrowser.open(x.codelink, '_blank', 'location=yes');
                if (window.cordova){
                    window.ga.trackEvent('הטבות שנלחצו', x.title);
                }

            } else {

                $state.go('app.item', {itemId:x.index});
                if (window.cordova){
                    window.ga.trackEvent('הטבות שנלחצו', x.title);
                }

            }

        };


        $scope.selection = 'catalog';

        $scope.chooseTab = function(x){$scope.selection = x;};

        $scope.weekAgo = new Date().setDate(new Date().getDate()-7);

        // get all favorites for the user

        $http.post($rootScope.host + 'GetUserFav', {"user" : $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
            .then(

            function(data){

                $scope.favorites = data.data;

                for(var j = 0; j < $scope.favorites.length; j++){

                    $rootScope.favoriteDeals.push($scope.favorites[j]);

                }

                console.log("Favs", $rootScope.favoriteDeals);

            },

            function(err){$ionicPopup.alert({title: "אין חיבור לרשת"});});

        // check if the deal is favorite

        $scope.isFavorite = function (x) {return isFavoriteFactory.isFavorite(x);};

        // make favorite

        $scope.makeFavorite = function(x, y){return makeFavoriteFactory.makeFavorite(x, $scope, y);};

        // delete favorite

        $scope.deleteFavorite = function(x, y){return deleteFavoriteFactory.deleteFavorite(x, $scope, y);};

        // if GPS is off and user wants to see the closest deals (turn on GPS and load deals with location);

        $scope.$watch('selection', function(){

            if ($scope.selection == 'location') {

                if ($rootScope.isLocationEnabled == false) {

                    $ionicPlatform.ready(function () {

                        document.addEventListener("deviceready", function () {

                            $ionicPopup.show({
                                template: '<div style="text-align: center">על מנת לקבל את הדילים הקרובים אלייך הדלק GPS</div>',
                                title: "",
                                scope: $scope,
                                buttons: [
                                    { text: 'Cancel' },
                                    {
                                        text: '<b>OK</b>',
                                        type: 'button-positive',
                                        onTap: function(e) {

                                            if (ionic.Platform.isIOS() == true){

                                                cordova.plugins.diagnostic.switchToSettings(

                                                    function(){     // success callback

                                                        // alert('win');

                                                    }, function(){      // error callback

                                                        $rootScope.getDealsWithoutLocation();

                                                    });

                                                document.addEventListener("resume", onResumeIOS, false);

                                                function onResumeIOS() {

                                                    CheckGPS.check(function win() {

                                                        var posOptions = {timeout: 3000, enableHighAccuracy: true};

                                                        $cordovaGeolocation
                                                            .getCurrentPosition(posOptions)
                                                            .then(function (position) {

                                                                $rootScope.lat = position.coords.latitude;
                                                                $rootScope.lng = position.coords.longitude;
                                                                $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                                                document.removeEventListener("resume", onResumeIOS);

                                                            }, function (err) {

                                                                $rootScope.getDealsWithoutLocation();

                                                                document.removeEventListener("resume", onResumeIOS);

                                                            });

                                                    }, function fail() {

                                                        $rootScope.getDealsWithoutLocation();

                                                        document.removeEventListener("resume", onResumeIOS);

                                                    })

                                                }

                                            } else {

                                                cordova.plugins.diagnostic.switchToLocationSettings();

                                                document.addEventListener("resume", onResume, false);

                                                function onResume() {

                                                    CheckGPS.check(function win() {

                                                        var posOptions = {timeout: 3000, enableHighAccuracy: true};

                                                        $cordovaGeolocation
                                                            .getCurrentPosition(posOptions)
                                                            .then(function (position) {

                                                                $rootScope.lat = position.coords.latitude;
                                                                $rootScope.lng = position.coords.longitude;
                                                                $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                                                document.removeEventListener("resume", onResume);

                                                            }, function (err) {

                                                                $rootScope.getDealsWithoutLocation();

                                                                document.removeEventListener("resume", onResume);

                                                            });

                                                        }, function fail() {

                                                            $rootScope.getDealsWithoutLocation();

                                                            document.removeEventListener("resume", onResume);

                                                        })

                                                }



                                            }


                                        }
                                    }
                                ]
                            });

                        })

                    }, false)

                } else if ($rootScope.isLocationEnabled == true) {

                    var posOptions = {timeout: 3000, enableHighAccuracy: true};

                    $cordovaGeolocation
                        .getCurrentPosition(posOptions)
                        .then(function (position) {

                            $rootScope.lat = position.coords.latitude;
                            $rootScope.lng = position.coords.longitude;

                            $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                        }, function (err) {

                            $rootScope.getDealsWithoutLocation();

                        });

                }
            }
        });

        // clicking on button Turn on GPS

        $scope.turnOnGPS = function(){

            // move user to settings

            if (ionic.Platform.isIOS() == true){

                cordova.plugins.diagnostic.switchToSettings(

                    function(){     // success callback

                        // alert('win2');

                    }, function(){      // error callback

                        $rootScope.getDealsWithoutLocation();

                    });

                document.addEventListener("resume", onResumeIOS, false);

                function onResumeIOS() {

                    CheckGPS.check(function win() {

                        var posOptions = {timeout: 3000, enableHighAccuracy: true};

                        $cordovaGeolocation
                            .getCurrentPosition(posOptions)
                            .then(function (position) {

                                $rootScope.lat = position.coords.latitude;
                                $rootScope.lng = position.coords.longitude;
                                $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                document.removeEventListener("resume", onResumeIOS);

                            }, function (err) {

                                $rootScope.getDealsWithoutLocation();

                                document.removeEventListener("resume", onResumeIOS);

                            });

                    }, function fail() {

                        $rootScope.getDealsWithoutLocation();

                        document.removeEventListener("resume", onResumeIOS);

                    })

                }

            } else {

                cordova.plugins.diagnostic.switchToLocationSettings();

                // listen to his return

                document.addEventListener("resume", onResume, false);

                function onResume() {

                    CheckGPS.check(function win() {     // if he turned on GPS

                        $ionicLoading.show({

                            template: 'טוען...'

                        }).then(function(){

                            var posOptions = {enableHighAccuracy: false};

                            $cordovaGeolocation
                                .getCurrentPosition(posOptions)
                                .then(function (position) {

                                    $rootScope.lat = position.coords.latitude;
                                    $rootScope.lng = position.coords.longitude;

                                    $rootScope.getDealsWithLocation($rootScope.lat, $rootScope.lng);

                                    $ionicLoading.hide();

                                    document.removeEventListener("resume", onResume);

                                }, function(err) {

                                    $rootScope.getDealsWithoutLocation();
                                    console.log('err1', err);

                                    $ionicLoading.hide();

                                    document.removeEventListener("resume", onResume);

                                });

                        })

                    }, function fail(){     // if he didn't turn on GPS

                        setTimeout(function() {

                            $ionicPopup.alert({
                                title: "לא הפעלת את הGPS - נא לנסות שנית",
                                buttons: [{
                                    text: 'OK',
                                    type: 'button-positive'
                                }]
                            });

                        }, 0);

                        document.removeEventListener("resume", onResume);

                    });

                }

            }

        };

    })

    .controller('ItemCtrl', function ($sce, isFavoriteFactory, makeFavoriteFactory, deleteFavoriteFactory, $scope, $stateParams, $rootScope, $state) {

        $scope.$on('$ionicView.enter', function(e) {

            $scope.deal = {};

            // choosing deal

            for(var i = 0; i < $rootScope.deals.length; i++){

                if ($stateParams.itemId == $rootScope.deals[i].index){

                    $scope.deal = $rootScope.deals[i];

                    if ($scope.deal.showiframe == "1"){

                        $scope.iframeLink = $sce.trustAsResourceUrl($scope.deal.codelink);

                    }

                    console.log($scope.deal);

                }

            }

            if(window.cordova){
                window.ga.trackView("הטבה" + " - " + $scope.deal.title);
            }

        });

        $scope.options = {
            loop: true,
            effect: 'slide',
            speed: 300,
            autoplay: 3000,
            pagination: false
        };

        $scope.goToLink = function(x, y){

            cordova.InAppBrowser.open(x, '_blank', 'location=yes');
            if(window.cordova) {
                window.ga.trackEvent('לינק בהטבה', y);
            }

        };

        // check if the deal is favorite

        $scope.isFavorite = function (x) {

            return isFavoriteFactory.isFavorite(x);

        };

        // make favorite

        $scope.makeFavorite = function(x, y){

            return makeFavoriteFactory.makeFavorite(x, $scope, y);

        };

    })

    .controller('InformationCtrl', function ($rootScope, DoubleClick, $scope, $ionicSideMenuDelegate, $state) {

        $scope.$on('$ionicView.enter', function(e) {

            if(ionic.Platform.isIOS()){

                DoubleClick.refreshAds('iosInformation');

            }  else if (ionic.Platform.isAndroid()){

                DoubleClick.refreshAds('androidInformation');

            }

            if(window.cordova)
                window.ga.trackView("מידע שימושי");

            if ($rootScope.banners.length !== 0){
                if ($rootScope.banners.info.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.info.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }
            }


        });

        $ionicSideMenuDelegate.canDragContent(false);

        $scope.options = {
            loop: true,
            effect: 'slide',
            speed: 300,
            autoplay: 3000,
            pagination: false
        };

    })

    .controller('ArticleCtrl', function ($ionicModal, $ionicScrollDelegate, $ionicSideMenuDelegate, $scope, $rootScope, $state, $stateParams, $http, $localStorage, $ionicPopup) {

        $ionicSideMenuDelegate.canDragContent(false);

        $scope.infoCategoryName = $rootScope.infoCategories[$stateParams.articleId - 1];
        $scope.infoCategoryIcon = "img/info_articles/" + $stateParams.articleId + ".png";
        $scope.itemImage = '';

        $scope.$on('$ionicView.enter', function(e) {

            // console.log($state);

            if(window.cordova)
                window.ga.trackView("מידע שימושי" + " - " + $scope.infoCategoryName);

            $scope.content = {};

            // get article for the page

            var send_data = {"catid" : $stateParams.articleId, "issoldier" : $localStorage.soldier};

            $http.post($rootScope.host + 'GetInfoArticles', send_data, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                .then(

                function(data){

                    $scope.content = data.data;
                    console.log("Articles", $scope.content);

                },

                function(err){$ionicPopup.alert({title: "אין חיבור לרשת"});});

        });

        $scope.showImageModal =  function (image) {

            $ionicModal.fromTemplateUrl('templates/modal_image.html', {scope: $scope, animation: 'slide-in-up'})

                .then(function(imageModal) {

                    $scope.itemImage = image;
                    $scope.imageModal = imageModal;
                    $scope.imageModal.show();

                });

        };

        // show video if needed

        $scope.showVideo = function (x){

            $scope.video = x;

            var videoPopup = $ionicPopup.show({
                templateUrl: 'templates/popup_video.html',
                scope: $scope,
                cssClass: 'popupVideo'
            });

            $scope.hideVideo = function () {

                videoPopup.close();

            };

        };

        // go to landing page if needed

        $scope.goToPage = function(x){

            cordova.InAppBrowser.open(x, '_blank', 'location=yes');

        };

        // scroll to top

        $scope.scrollTopArticle = function () {

            $ionicScrollDelegate.scrollTop('shouldAnimate');

        };

    })

    .controller('QuestionsCtrl', function($timeout, $ionicLoading, DoubleClick, $ionicPopup, $httpParamSerializerJQLike, $interval, $state, $localStorage, $rootScope, $http, $scope, $stateParams){

        $scope.$on('$ionicView.beforeEnter', function() {

            if(window.cordova){

                switch ($state.current.name){

                    case 'app.inter':
                        window.ga.trackView("עמוד מעבר " + (Number($stateParams.questionId) + 1));
                        break;

                    case 'app.questions':
                        window.ga.trackView("עמוד שאלה " + (Number($stateParams.questionId) + 1));
                        break;

                    case 'app.answers':
                        window.ga.trackView("עמוד תשובה " + (Number($stateParams.questionId) + 1));
                        break;

                    case 'app.results':
                        window.ga.trackView("עמוד תוצאות");
                        break;

                    case 'default':
                        break;
                }

            }

            $scope.clock = 45;
            $scope.points = $rootScope.divider * $scope.clock / 45;
            $scope.points = Math.round( $scope.points * 10 ) / 10;
            $scope.customHeight = 'height: 40px !important;';

            if ($state.current.name === "app.inter"){

                switch ($stateParams.questionId){

                    case '0':

                        if(ionic.Platform.isIOS()){

                            DoubleClick.refreshAds('inter1Ios');
                            DoubleClick.refreshAds('inter1CubeIos');

                        }  else if (ionic.Platform.isAndroid()){

                            DoubleClick.refreshAds('inter1Android');
                            DoubleClick.refreshAds('inter1CubeAndroid');

                        }

                        if ($rootScope.banners.length !== 0){

                            if ($rootScope.banners.banner1.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.banner1.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }

                            if ($rootScope.banners.cube1.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.cube1.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        $http.post($rootScope.host + 'getDriverPopupDate', {user: $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})

                            .then(function (data) {

                                if ((data.data.date == null) || (moment().diff(moment(data.data.date, 'YYYY-MM-DD 00:00:00'), 'days') > 3)){

                                    $scope.monthDriverPopup = $ionicPopup.show({
                                        templateUrl: 'templates/popup_month_driver.html',
                                        scope: $scope,
                                        cssClass: 'prizesPopup'});

                                    $http.post($rootScope.host + 'updateDriverPopupDate', {
                                        user: $localStorage.userid,
                                        date: moment().format('YYYY-MM-DD')
                                    }, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                                }

                            });
                        break;

                    case '1':

                        if(ionic.Platform.isIOS()){

                            DoubleClick.refreshAds('inter2Ios');
                            DoubleClick.refreshAds('inter2CubeIos');

                        }  else if (ionic.Platform.isAndroid()){

                            DoubleClick.refreshAds('inter2Android');
                            DoubleClick.refreshAds('inter2CubeAndroid');

                        }

                        if ($rootScope.banners.length !== 0){

                            if ($rootScope.banners.banner2.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.banner2.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }

                            if ($rootScope.banners.cube2.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.cube2.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        break;

                    case '2':

                        if(ionic.Platform.isIOS()){

                            DoubleClick.refreshAds('inter3Ios');
                            DoubleClick.refreshAds('inter3CubeIos');

                        }  else if (ionic.Platform.isAndroid()){

                            DoubleClick.refreshAds('inter3Android');
                            DoubleClick.refreshAds('inter3CubeAndroid');

                        }

                        if ($rootScope.banners.length !== 0){

                            if ($rootScope.banners.banner3.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.banner3.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }

                            if ($rootScope.banners.cube3.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.cube3.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        break;

                    case '3':

                        if(ionic.Platform.isIOS()){

                            DoubleClick.refreshAds('inter4Ios');
                            DoubleClick.refreshAds('inter4CubeIos');

                        }  else if (ionic.Platform.isAndroid()){

                            DoubleClick.refreshAds('inter4Android');
                            DoubleClick.refreshAds('inter4CubeAndroid');

                        }

                        if ($rootScope.banners.length !== 0){

                            if ($rootScope.banners.banner4.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.banner4.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }

                            if ($rootScope.banners.cube4.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.cube4.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        break;

                    case '4':

                        if(ionic.Platform.isIOS()){

                            DoubleClick.refreshAds('inter5Ios');
                            DoubleClick.refreshAds('inter5CubeIos');

                        }  else if (ionic.Platform.isAndroid()){

                            DoubleClick.refreshAds('inter5Android');
                            DoubleClick.refreshAds('inter5CubeAndroid');

                        }

                        if ($rootScope.banners.length !== 0){

                            if ($rootScope.banners.banner5.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.banner5.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }

                            if ($rootScope.banners.cube5.TipliBannerType === '0'){
                                angular.forEach($rootScope.banners.cube5.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                            }
                        }

                        break;

                    case 'default':
                        break;

                }


            }

            if ($state.current.name === "app.questions"){

                $rootScope.question = $rootScope.questions[$stateParams.questionId];
                console.log("Question", $rootScope.question);

                if ($rootScope.question.correct_answer == "1"){

                    $rootScope.rightAnswer = $rootScope.question.answer1;

                } else if($rootScope.question.correct_answer == "2"){

                    $rootScope.rightAnswer = $rootScope.question.answer2;

                } else if($rootScope.question.correct_answer == "3"){

                    $rootScope.rightAnswer = $rootScope.question.answer3;

                } else if($rootScope.question.correct_answer == "4"){

                    $rootScope.rightAnswer = $rootScope.question.answer4;

                }

            }

            if ($state.current.name === "app.results"){

                if(ionic.Platform.isIOS()){

                    DoubleClick.refreshAds('resultsCubeIos');

                }  else if (ionic.Platform.isAndroid()){

                    DoubleClick.refreshAds('resultsCubeAndroid');

                }

                if ($rootScope.banners.length !== 0){

                    if ($rootScope.banners.cube_results.TipliBannerType === '0'){
                        angular.forEach($rootScope.banners.cube_results.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                    }

                }

            }

            $scope.questionDate = $stateParams.date;
            $scope.allAnswers = [$rootScope.question.answer1, $rootScope.question.answer2, $rootScope.question.answer3, $rootScope.question.answer4];
            $rootScope.questionId = $stateParams.questionId;
            $rootScope.questionNumber = Number($rootScope.questionId) + 1;
        });

        // which sound should be?

        $scope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, options) {

            if (fromState.name === "app.questions" && toState.name === 'app.answers'){

                angular.forEach($rootScope.answers, function(answer){

                    if (answer.id === $stateParams.questionId){

                        if (answer.is_correct == true){

                            var audio = new Audio('sounds/yes-sound.wav');
                            audio.play();

                        } else {

                            var audio = new Audio('sounds/no-sound.wav');
                            audio.play();

                        }

                    }

                });

            }

        });

        $scope.userAnswer = {selected : ''};
        $scope.points = '';
        $scope.timer = true;
        $scope.name = $localStorage.firstname + ' ' + $localStorage.lastname;

        $scope.selectAnswer = selectAnswer;
        $scope.isAnswerSelected = isAnswerSelected;
        $scope.nextQuestion = nextQuestion;
        $scope.eraseAnswers = eraseAnswers;
        $scope.shareFB = shareFB;
        $scope.share = share;
        $scope.goToResults = goToResults;

        $interval(function () {

            if ($scope.clock > 0 && $scope.timer){

                $scope.clock -= 1;
                $scope.points = $rootScope.divider * $scope.clock / 45;
                $scope.points = Math.round( $scope.points * 10 ) / 10;
                return $scope.points;

            } else
                return 0;

            }, 1000);

        function selectAnswer(x){

            if ($scope.userAnswer.selected !== '')
                return;

            $scope.userAnswer.selected = x;

            $scope.timer = false;

            $rootScope.answers.push({
                question_date: $stateParams.date,
                user_id: $localStorage.userid,
                id: $stateParams.questionId,
                question_id: $rootScope.question.index,
                question: $rootScope.question,
                user_answer: $scope.userAnswer.selected,
                is_correct: $scope.userAnswer.selected == $rootScope.question.correct_answer,
                correct_answer: $scope.rightAnswer,
                points: $scope.userAnswer.selected == $rootScope.question.correct_answer ? $scope.points : 0,
                is_bonus: 0,
                clock: $scope.clock,
                session: $rootScope.session
            });

            $rootScope.rightAnswers = $scope.userAnswer.selected == $rootScope.question.correct_answer ? $rootScope.rightAnswers + 1 : $rootScope.rightAnswers;
            console.log("rightAnswers", $rootScope.rightAnswers);
            $rootScope.isAnswerCorrect = $scope.userAnswer.selected == $rootScope.question.correct_answer;
            console.log("Answers", $rootScope.answers);

            $http.post($rootScope.host + 'NewAddUserPoints', $httpParamSerializerJQLike({object_data:
                [{
                    question_date: $stateParams.date,
                    user_id: $localStorage.userid,
                    id: $stateParams.questionId,
                    question_id: $rootScope.question.index,
                    question: $rootScope.question,
                    user_answer: $scope.userAnswer.selected,
                    is_correct: $scope.userAnswer.selected == $rootScope.question.correct_answer,
                    correct_answer: $scope.rightAnswer,
                    points: $scope.userAnswer.selected == $rootScope.question.correct_answer ? $scope.points : 0,
                    is_bonus: 0,
                    clock: $scope.clock,
                    session: $rootScope.session
                }]
            }), {
                headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'},
                paramSerializer: '$httpParamSerializerJQLike'
            })
                .then(function (data) {

                    $ionicLoading.hide();
                    $rootScope.newPoints = data.data.points;

                });

            $timeout(function(){$state.go('app.answers', {date: $stateParams.date, questionId : $stateParams.questionId})}, 3000);

        }

       function isAnswerSelected(x){

         if ($scope.userAnswer.selected == x)
             return true;

       }

        function nextQuestion() {

            $rootScope.divider += 20;
            $scope.userAnswer = {selected : ''};
            $state.go('app.inter', {date: $stateParams.date, questionId : Number($rootScope.questionId) + 1 });

        }

        function goToResults() {

            $rootScope.getUserMonth().then(function () {
                $state.go('app.results');
            });


        }

        function eraseAnswers(x) {

             if (x === 1){

                 $localStorage.once = 0;
                 $rootScope.once = $localStorage.once;

             } else {

                 $localStorage.twice = 0;
                 $rootScope.twice = $localStorage.twice;

             }


            for (var j = 0; j < x; j++){

                var erased_answer = $scope.allAnswers[Math.floor(Math.random()*$scope.allAnswers.length)];

                if (typeof erased_answer !== 'undefined'){

                    for (var i = 0; i < $scope.allAnswers.length; i++) {

                        if ($scope.allAnswers[i] === $rootScope.rightAnswer && $scope.allAnswers[i] === erased_answer){
                            x++;
                            continue;
                        }

                        if ($scope.allAnswers[i] === erased_answer && $scope.allAnswers[i] !== $rootScope.rightAnswer){

                            if ($scope.allAnswers[Number($scope.userAnswer.selected)] === erased_answer)
                                $scope.userAnswer.selected = '';

                            delete $scope.allAnswers[i];

                        }

                    }

                } else
                    x++;

            }

        }

        function shareFB() {

            window.plugins.socialsharing.shareViaFacebookWithPasteMessageHint(

                'גם אני עונה כל יום באפליקציה tip-li על שאלות בנהיגה נכונה בשביל לזכות ב-28 שעורי נהיגה חינם או בחופשה זוגית. תורידו עכשיו',
                null /* img */,
                'http://tip-li.co.il/redirection',
                'גם אני עונה כל יום באפליקציה tip-li על שאלות בנהיגה נכונה בשביל לזכות ב-28 שעורי נהיגה חינם או בחופשה זוגית. תורידו עכשיו',
                function(result) {console.log("success");},
                function(msg) {console.log("fail")}

            );

        }

        function share() {

            var options = {
                message: 'גם אני עונה כל יום באפליקציה tip-li על שאלות בנהיגה נכונה בשביל לזכות ב-28 שעורי נהיגה חינם או בחופשה זוגית. תורידו עכשיו',
                url: 'http://tip-li.co.il/redirection'
            };

            var onSuccess = function(result) {console.log("success");};

            var onError = function(msg) {console.log("fail");};

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);

        }

    })

    .controller('LeaderboardCtrl', function ($ionicPopup, $http, $scope, $rootScope, $localStorage) {

        $scope.$on('$ionicView.beforeEnter', function() {

            $http.post($rootScope.host + 'getLeaders', {'user' : $localStorage.userid}, {headers: {'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8; application/json'}})
                .then(function (data) {

                    $scope.leaders = data.data.leaders;
                    $scope.rank = data.data.rank;
                    console.log('Leaderboard', $scope.leaders, $scope.rank);

                },

                    function (err) {$ionicPopup.alert({title: "אין חיבור לרשת"});})

        });

        $scope.leaders = [];
        $scope.rank = 0;

        $scope.share = share;

        function share() {

            var options = {
                message: 'גם אני עונה כל יום באפליקציה tip-li על שאלות בנהיגה נכונה בשביל לזכות ב-28 שעורי נהיגה חינם או בחופשה זוגית. תורידו עכשיו',
                url: 'http://tip-li.co.il/redirection'
            };

            var onSuccess = function(result) {console.log("success");};

            var onError = function(msg) {console.log("fail");};

            window.plugins.socialsharing.shareWithOptions(options, onSuccess, onError);

        }

    })

    .controller('MonthPrizeCtrl', function($scope, $rootScope){

        $scope.$on('$ionicView.enter', function() {

            if ($rootScope.banners.length !== 0){

                if ($rootScope.banners.banner_28.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.banner_28.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }

                if ($rootScope.banners.banner_weekend.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.banner_weekend.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }

                if ($rootScope.banners.transition_28.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.transition_28.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }

                if ($rootScope.banners.transition_weekend.TipliBannerType === '0'){
                    angular.forEach($rootScope.banners.transition_weekend.banners, function (banner) {$rootScope.sendShownBanner(banner.index)})
                }
            }

        })

    })


    .directive('iframe', function () {
        return {
            compile: function (element) {
                element.attr('target', '_blank');
            }
        };

    })

    .controller('TestCtrl', function($state, $scope, $rootScope, $stateParams, $timeout){

        $scope.$on('$ionicView.enter', function() {

            $scope.test_id = $stateParams.test_id;
            $scope.question_number = Number($scope.test_id) + 1;
            $scope.question = $rootScope.testQuestions[$scope.test_id];

        });

        $scope.test_id = $stateParams.test_id;
        $scope.question_number = Number($scope.test_id) + 1;
        $scope.question = $rootScope.testQuestions[$scope.test_id];
        $scope.back = ['#A3DAF9', '#A3DAF9', '#A3DAF9', '#A3DAF9'];
        $scope.answer = '';

        $scope.select = function (x) {

            $scope.answer = x;

            if (x == $scope.question.correct_answer){

                $rootScope.testRightAnswers += 1;
                console.log('testRightAnswers', $rootScope.testRightAnswers);
                $scope.back[x-1] = '#7DC622';

            } else {

                $scope.back[x-1] = '#D9EDF8';
                $scope.back[$scope.question.correct_answer-1] = '#7DC622';

            }

        };

        $scope.goToNext = function () {

            if ($scope.answer !== '')
                $state.go('app.test', {test_id: $scope.question_number});

        }

    })

;